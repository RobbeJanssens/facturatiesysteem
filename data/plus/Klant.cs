﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem.data {
	public partial class Klant {
		public override string ToString( ) => Naam;
		public string Naam => $"{Voornaam} {Achternaam}";
	}

}

