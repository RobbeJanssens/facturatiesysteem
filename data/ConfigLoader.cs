﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Facturatiesysteem.data {
	class ConfigLoader {
		//
		// Fields
		//
		private readonly string xmlFile = Environment.CurrentDirectory + @"\settings.xml";

		//
		// Constructors
		//
		public ConfigLoader( ) {
			LoadSettings( );
		}

		//
		// Properties
		//
		public Settings Settings { get; private set; }
		private string SettingsFile => xmlFile;

		//
		// Methodes
		//
		public void LoadSettings( ) {
			XmlSerializer serializer = new XmlSerializer(typeof(Settings));

			using Stream reader = new FileStream(SettingsFile, FileMode.Open);
			Settings = (Settings)serializer.Deserialize(reader);
		}
	}

	public class Settings {
		[XmlElement]
		public CompanyInfo CompanyInfo { get; set; }

		[XmlElement]
		public string InvoiceSpecialMessage { get; set; }
	}

	public class CompanyInfo {
		[XmlElement]
		public string Name { get; set; }

		[XmlElement]
		public string Adress { get; set; }

		[XmlElement]
		public string Telephone { get; set; }

		[XmlElement]
		public string Email { get; set; }

		[XmlElement]
		public string Website { get; set; }

		[XmlElement]
		public FinacialInfo FinacialInfo { get; set; }

		[XmlElement]
		public string RPR { get; set; }
	}

	public class FinacialInfo {
		[XmlElement]
		public string BTWNummer { get; set; }

		[XmlElement]
		public string IBAN { get; set; }

		[XmlElement]
		public string BIC { get; set; }
	}
}
