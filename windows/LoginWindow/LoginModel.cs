﻿using Facturatiesysteem.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem {
	class LoginModel {
		//
		// Fields
		//

		//
		// Constructors
		//
		public LoginModel( ) {

		}

		//
		// Properties
		//

		//
		// Methodes
		//

		// Deze method geeft een personeelslid terug als er juist is ingelogd anders null
		public Personeelslid CheckLogin(string login, string pass) {
			Personeelslid p = DataManager.GetPersoneelslidByLogin(login);
			if (p is null) return null;
			if (p.Password == pass) return p;
			else return null;
		}
	}
}
