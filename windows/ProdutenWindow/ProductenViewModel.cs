﻿using Facturatiesysteem.data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Effects;

namespace Facturatiesysteem.windows.ProdutenWindow {
	class ProductenViewModel : ViewModelBase {
		//
		// Fields
		//
		private List<Product> _producten;
		private List<Categorie>  _categorieen;

		private string _txtNaam;
		private Categorie _selectedCategorie;
		private int _selectedIndexCategorieen;
		private string _txtInkoopprijs;
		private string _txtVerkoopprijs;
		private string _txtEenheid;
		private string _txtBTW;
		private string _txtBeschrijving;

		//
		// Constructors
		//
		public ProductenViewModel(Personeelslid personeelslid) {
			RefreshCommand = new Command(new Action<object>(Refresh));
			CopyTextCommand = new Command(new Action<object>(CopyText));
			CloseCommand = new Command(new Action<object>(Close));
			DeleteProductCommand = new Command(new Action<object>(DeleteProdcut));
			InsertNewProductCommand = new Command(new Action<object>(InsertProduct));
			UpdateProductCommand = new Command(new Action<object>(UpdateProduct));

			Personeelslid = personeelslid;
			RefreshAll( );

			ErrorCheckList["TxtInkoopprijs"] = Validation.StringMustBeNumberOrNull;
			ErrorCheckList["TxtVerkoopprijs"] = Validation.StringMustBeNumberOrNull;
			ErrorCheckList["TxtBTW"] = Validation.StringMustBeNumberOrNull;
		}

		//
		// Properties
		//
		public Personeelslid Personeelslid { get; private set; }
		public List<Product> Producten {
			get => _producten;
			set {
				_producten = value;
				NotifyPropertyChanged( );
			}
		}
		public List<Categorie> Categorieen {
			get => _categorieen;
			set {
				_categorieen = value;
				NotifyPropertyChanged( );
			}
		}

		public Command RefreshCommand { get; set; }
		public Command CloseCommand { get; set; }
		public Command CopyTextCommand { get; set; }
		public Command InsertNewProductCommand { get; set; }
		public Command UpdateProductCommand { get; set; }
		public Command DeleteProductCommand { get; set; }

		#region Data Bindings
		public string TxtNaam {
			get => _txtNaam;
			set {
				_txtNaam = value;
				NotifyPropertyChanged( );
			}
		}
		public Categorie SelectedCategorie {
			get => _selectedCategorie;
			set {
				_selectedCategorie = value;
				NotifyPropertyChanged( );
			}
		}
		public int SelectedIndexCategorieen {
			get => _selectedIndexCategorieen;
			set {
				_selectedIndexCategorieen = value;
				NotifyPropertyChanged( );
			}
		}
		public string TxtInkoopprijs {
			get => _txtInkoopprijs;
			set {
				_txtInkoopprijs = value;
				NotifyPropertyChanged( );
			}
		}
		public string TxtVerkoopprijs {
			get => _txtVerkoopprijs;
			set {
				_txtVerkoopprijs = value;
				NotifyPropertyChanged( );
			}
		}
		public string TxtEenheid {
			get => _txtEenheid;
			set {
				_txtEenheid = value;
				NotifyPropertyChanged( );
			}
		}
		public string TxtBTW {
			get => _txtBTW;
			set {
				_txtBTW = value;
				NotifyPropertyChanged( );
			}
		}
		public string TxtBeschrijving {
			get => _txtBeschrijving;
			set {
				_txtBeschrijving = value;
				NotifyPropertyChanged( );
			}
		}
		#endregion

		//
		// Methodes
		//
		private void RefreshAll( ) {
			Producten = DataManager.GetAllProducten( );
			Categorieen = DataManager.GetAllCategorieen( );

			TxtNaam = "";
			TxtBeschrijving = "";
			TxtBTW = "";
			TxtEenheid = "";
			TxtInkoopprijs = "";
			TxtVerkoopprijs = "";
		}

		public void InsertProduct(object obj) {
			if (!Personeelslid.Group.HasPermission(Permission.InsertProduct)) {
				MessageBox.Show("Je hebt heen toegang om een product toe te voegen.", "Fout");
				return;
			}
			if (SelectedCategorie is null)
				MessageBox.Show("Je hebt geen categorie geslecteerd", "Fout");
			else {
				Product p = new Product {
					ProductID = Guid.NewGuid(),
					Naam = TxtNaam,
					Inkoopprijs = string.IsNullOrWhiteSpace(TxtInkoopprijs) ? 0M : decimal.Parse(TxtInkoopprijs, NumberStyles.Any),
					Verkoopprijs = string.IsNullOrWhiteSpace(TxtVerkoopprijs) ? 0M : decimal.Parse(TxtVerkoopprijs, NumberStyles.Any),
					Eenheid = TxtEenheid,
					BTW = string.IsNullOrWhiteSpace(TxtBTW) ? 0M : decimal.Divide(decimal.Parse(TxtBTW, NumberStyles.Any), 100M),
					Beschrijving = TxtBeschrijving,
					CategorieID = SelectedCategorie.CategorieID
				};
				DataManager.InsertProdcut(p);
				RefreshAll( );
			}
		}
		public void UpdateProduct(object grid) {
			if (!Personeelslid.Group.HasPermission(Permission.UpdateProduct)) {
				MessageBox.Show("Je hebt heen toegang om een product te verwijderen.", "Fout");
				return;
			}
			Product product = (Product)((DataGrid)grid).SelectedItem;
			if (product is null)
				MessageBox.Show("Je hebt geen product geslecteerd.", "Fout");
			else if (SelectedCategorie is null)
				MessageBox.Show("Je hebt geen categorie geslecteerd", "Fout");
			else {
				Product p = new Product {
					ProductID = product.ProductID,
					Naam = TxtNaam,
					Inkoopprijs = string.IsNullOrWhiteSpace(TxtInkoopprijs) ? 0M : decimal.Parse(TxtInkoopprijs, NumberStyles.Any),
					Verkoopprijs = string.IsNullOrWhiteSpace(TxtVerkoopprijs) ? 0M : decimal.Parse(TxtVerkoopprijs, NumberStyles.Any),
					Eenheid = TxtEenheid,
					BTW = string.IsNullOrWhiteSpace(TxtBTW) ? 0M : decimal.Divide(decimal.Parse(TxtBTW, NumberStyles.Any), 100M),
					Beschrijving = TxtBeschrijving,
					CategorieID = SelectedCategorie.CategorieID
				};
				DataManager.UpdateProdcut(p);
				RefreshAll( );
			}
		}
		public void DeleteProdcut(object grid) {
			if (!Personeelslid.Group.HasPermission(Permission.DeleteProduct)) {
				MessageBox.Show("Je hebt heen toegang om een product te verwijderen.", "Fout");
				return;
			}
			Product product = (Product)((DataGrid)grid).SelectedItem;
			if (product is null)
				MessageBox.Show("Je hebt geen product geslecteerd.", "Fout");
			else if (DataManager.GetBoundBestellingProductCountFromProduct(product) != 0)
				MessageBox.Show("Het geselecteerde product is al gebruikt in bestellingen en kan dus niet verwijderd worden.", "Fout");
			else {
				MessageBoxResult result = MessageBox.Show($"Ben je zeker dat je {product.Naam} wilt verwijderen?", "Waarschuwing", MessageBoxButton.YesNoCancel);
				if (result == MessageBoxResult.Yes) {
					DataManager.DeleteProduct(product);
					RefreshAll( );
				}
			}
		}
		public void Refresh(object obj) {
			RefreshAll( );
		}
		public void CopyText(object grid) {
			Product product  = (Product)((DataGrid)grid).SelectedItem;
			if (product is null)
				MessageBox.Show("Je hebt geen product geselecteerd!", "Fout");
			else {
				TxtNaam = product.Naam;
				SelectedIndexCategorieen = Categorieen.FindIndex(c => c.CategorieID == product.CategorieID);
				TxtInkoopprijs = $"{product.Inkoopprijs:f2}";
				TxtVerkoopprijs = $"{product.Verkoopprijs:f2}";
				TxtEenheid = product.Eenheid;
				TxtBTW = $"{product.BTW * 100:f1}";
				TxtBeschrijving = product.Beschrijving;
			}
		}
		public void Close(object window) {
			((Window)window).Close( );
		}
	}
}
