﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem.data {
	public partial class Korting {
		public string HoeveelheidPercent => $"{Hoeveelheid * 100:f1}%";
	}
}
