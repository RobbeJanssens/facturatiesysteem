﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem.data {
	public partial class BestellingProduct {
		public decimal Subtotal => decimal.Multiply((decimal)PrijsPerUnit, (decimal)Aantal);
	}
}
