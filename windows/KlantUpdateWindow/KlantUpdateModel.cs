﻿using Facturatiesysteem.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem {
	class KlantUpdateModel {
		//
		// Fields
		//

		//
		// Constructors
		//
		public KlantUpdateModel(Klant k) {
			if (k.Adres is null) {
				k.Adres = new Adres( ) { AdresID = Guid.NewGuid( ) };
			}

			Klant = k;
		}

		//
		// Properties
		//
		public Klant Klant { get; set; }

		//
		// Methodes
		//
		public bool UpdateKlant( ) {
			return DataManager.UpdateKlant(Klant);
		}
	}
}
