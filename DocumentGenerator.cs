﻿using Apitron.PDF.Kit;
using Apitron.PDF.Kit.FixedLayout;
using Apitron.PDF.Kit.FixedLayout.PageProperties;
using Apitron.PDF.Kit.FixedLayout.Resources;
using Apitron.PDF.Kit.FixedLayout.Resources.Fonts;
using Apitron.PDF.Kit.FlowLayout.Content;
using Apitron.PDF.Kit.Styles;
using Apitron.PDF.Kit.Styles.Appearance;
using Apitron.PDF.Kit.Styles.Text;
using Facturatiesysteem.data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem {
	class DocumentGenerator {

		public static void GenerateInvoiceAndSave(string filename, Bestelling bestelling) {
			using Stream stream = new FileStream(filename, FileMode.Create);
			GenerateInvoice(stream, bestelling);
		}

		public static void GenerateInvoice(Stream stream, Bestelling bestelling) {
			Settings settings = new ConfigLoader().Settings;

			FlowDocument document = new FlowDocument();
			string imagesPath = Environment.CurrentDirectory + @"\images\";

			document.StyleManager.RegisterStyle("gridrow.tableHeader", new Style( ) { Background = RgbColors.LightSlateGray });
			document.StyleManager.RegisterStyle("gridrow.centerAlignedCells > *,gridrow > *.centerAlignedCell", new Style( ) { Align = Align.Center, Margin = new Thickness(0) });
			document.StyleManager.RegisterStyle("gridrow > *.leftAlignedCell", new Style( ) { Align = Align.Left, Margin = new Thickness(5, 0, 0, 0) });
			document.StyleManager.RegisterStyle("gridrow > *", new Style( ) { Align = Align.Right, Margin = new Thickness(0, 0, 5, 0) });
			document.StyleManager.RegisterStyle("textblock", new Style( ) { Font = new Apitron.PDF.Kit.Styles.Text.Font(StandardFonts.Helvetica, 11) });

			ResourceManager resourceManager = new ResourceManager();
			resourceManager.RegisterResource(
				new Apitron.PDF.Kit.FixedLayout.Resources.XObjects.Image("logo",
				Path.Combine(imagesPath, "logo.png"), true) { Interpolate = true });

			document.PageHeader.Margin = new Thickness(0, 40, 0, 0);
			document.PageHeader.Padding = new Thickness(10, 0, 10, 0);
			document.PageHeader.Height = 110;
			document.PageHeader.Background = RgbColors.LightGray;
			document.PageHeader.LineHeight = 60;
			document.PageHeader.Add(new Image("logo") { Height = 60, Width = 60, VerticalAlign = VerticalAlign.Middle });
			document.PageHeader.Add(new TextBlock("Factuur") {
				Display = Display.InlineBlock,
				Align = Align.Right,
				Font = new Apitron.PDF.Kit.Styles.Text.Font(StandardFonts.CourierBold, 20),
				Color = RgbColors.Black
			});
			Section pageSection = new Section() {Padding = new Thickness(20,10,20,10)};

			Grid invoiceInfo = new Grid(Length.Auto, Length.Auto) {InnerBorderColor = Color.Transparent, BorderColor = Color.Transparent, Padding = new Thickness(0,0,0,20)};
			double fontSize = 9;
			Apitron.PDF.Kit.Styles.Text.Font font = new Apitron.PDF.Kit.Styles.Text.Font(StandardFonts.Courier, fontSize);
			CompanyInfo info = settings.CompanyInfo;
			Section companyInfo = new Section(){ Class = "leftAlignedCell"};
			companyInfo.Add(new TextBlock($"{info.Name}") { Font = font });
			companyInfo.Add(new Br( ));
			companyInfo.Add(new TextBlock($"{info.Adress}") { Font = font });
			companyInfo.Add(new Br( ));
			companyInfo.Add(new TextBlock($"Tel: {info.Telephone}") { Font = font });
			companyInfo.Add(new Br( ));
			companyInfo.Add(new TextBlock($"e-mail: {info.Email}") { Font = font });
			companyInfo.Add(new Br( ));
			companyInfo.Add(new TextBlock($"{info.Website}") { Font = font });
			companyInfo.Add(new Br( ));
			companyInfo.Add(new TextBlock($"BTW: {info.FinacialInfo.BTWNummer}") { Font = font });
			companyInfo.Add(new Br( ));
			companyInfo.Add(new TextBlock($"IBAN: {info.FinacialInfo.IBAN}") { Font = font });
			companyInfo.Add(new Br( ));
			companyInfo.Add(new TextBlock($"BIC: {info.FinacialInfo.BIC}") { Font = font });
			companyInfo.Add(new Br( ));
			companyInfo.Add(new TextBlock($"RPR: {info.RPR}") { Font = font });
			Section customerInfo = new Section() { Class = "leftAlignedCell"};
			customerInfo.Add(new TextBlock($"Factuur voor:") { Font = font });
			customerInfo.Add(new Br( ));
			customerInfo.Add(new TextBlock($"{bestelling.Klant.Naam}") { Font = font });
			customerInfo.Add(new Br( ));
			customerInfo.Add(new TextBlock($"{bestelling.Klant.Adres}") { Font = font });

			invoiceInfo.Add(new GridRow(companyInfo, customerInfo));

			pageSection.Add(invoiceInfo);

			DateTime vervaldatum = (DateTime)bestelling.DatumFactuur;
			vervaldatum = vervaldatum.AddDays(Convert.ToDouble(bestelling.FactuurBetaalTermein is null ? 0 : bestelling.FactuurBetaalTermein));
			if (bestelling.Klant.BTWNummer is null)
				pageSection.Add(new Grid(Length.Auto, Length.Auto, Length.Auto) {
					new GridRow(new TextBlock("Factuurnummer") {Class = "centerAlignedCell"}, new TextBlock("Datum") {Class = "centerAlignedCell"}, new TextBlock("Vervaldatum") {Class = "centerAlignedCell"}),
					new GridRow(new TextBlock($"{bestelling.DatumFactuur:yyyy}-{bestelling.BestellingNummer}") {Class = "centerAlignedCell"}, new TextBlock($"{bestelling.DatumFactuur:yyyy-MM-dd}") {Class = "centerAlignedCell"}, new TextBlock($"{vervaldatum:yyyy-MM-dd}") {Class = "centerAlignedCell"}),
				});
			else
				pageSection.Add(new Grid(Length.Auto, Length.Auto, Length.Auto, Length.Auto) {
					new GridRow(new TextBlock("Factuurnummer") {Class = "centerAlignedCell"}, new TextBlock("Datum") {Class = "centerAlignedCell"}, new TextBlock("Vervaldatum") {Class = "centerAlignedCell"}, new TextBlock("BTW Nummer") {Class = "centerAlignedCell"}),
					new GridRow(new TextBlock($"{bestelling.DatumFactuur:yyyy}-{bestelling.BestellingNummer}") {Class = "centerAlignedCell"}, new TextBlock($"{bestelling.DatumFactuur:yyyy-MM-dd}") {Class = "centerAlignedCell"}, new TextBlock($"{vervaldatum:yyyy-MM-dd}") {Class = "centerAlignedCell"}, new TextBlock($"{bestelling.Klant.BTWNummer}") {Class = "centerAlignedCell"}),
				});

			pageSection.Add(new Hr( ) { Padding = new Thickness(0, 0, 0, 20) });
			pageSection.Add(CreateProductsGrid(bestelling));

			if (!string.IsNullOrEmpty(settings.InvoiceSpecialMessage)) {
				pageSection.Add(new Br( ));
				pageSection.Add(new TextBlock($"{settings.InvoiceSpecialMessage}"));
			}

			document.Add(pageSection);
			document.Write(stream, resourceManager, new PageBoundary(Boundaries.A4));
		}

		private static Grid CreateProductsGrid(Bestelling bestelling) {
			Grid productsGrid = new Grid(20, Length.Auto, 40, 50, 60, 50) {
				new GridRow(new TextBlock("#"), new TextBlock("Product"), new TextBlock("Aantal"), new TextBlock("Prijs"), new TextBlock("Total"), new TextBlock("BTW")) { Class = "tableHeader centerAlignedCells" }
			};

			// alle producten toevoegen
			int i = 1;
			foreach (BestellingProduct bp in bestelling.BestellingProducten) {
				Product product = bp.Product;
				TextBlock pos = new TextBlock(i++.ToString()) {Class = "centerAlignedCell"};
				Section description;
				if (string.IsNullOrWhiteSpace(bp.Opmerking))
					description = new Section(new TextBlock(product.Naam)) { Class = "leftAlignedCell" };
				else
					description = new Section(
						new TextBlock(product.Naam),
						new Br( ),
						new TextBlock(bp.Opmerking) { Font = new Apitron.PDF.Kit.Styles.Text.Font(StandardFonts.Helvetica, 10), Color = RgbColors.Gray }) { Class = "leftAlignedCell" };
				TextBlock qty = new TextBlock(bp.Aantal.ToString()) {Class = "centerAlignedCell"};
				TextBlock price = new TextBlock( $"{bp.PrijsPerUnit:f2}");
				TextBlock total = new TextBlock($"{bp.Subtotal:f2}");
				TextBlock btw = new TextBlock($"{product.BTWPercent}");
				productsGrid.Add(new GridRow(pos, description, qty, price, total, btw));
			}

			Dictionary<Korting, decimal> kortingen = bestelling.GetKortingen();
			if (kortingen.Count > 0) {
				// prijs zonder korting
				productsGrid.Add(new GridRow(new TextBlock("Subtotaal") { ColSpan = 4 }, new TextBlock($"€ {bestelling.Subtotaal:f2}") { ColSpan = 2 }));
				// alle kortingen
				foreach (KeyValuePair<Korting, decimal> kp in kortingen) {
					productsGrid.Add(new GridRow(new TextBlock(kp.Key.Beschrijving) { ColSpan = 4 }, new TextBlock($"€ -{kp.Value:f2}") { ColSpan = 2 }));
				}
			}

			// subtotaal met korting
			productsGrid.Add(new GridRow(new TextBlock("Totaal zonder BTW") { ColSpan = 4 }, new TextBlock($"€ {bestelling.TotaalPrijs:f2}") { ColSpan = 2 }));

			// alle taxes er bij toevoegen
			foreach (KeyValuePair<string, decimal> kp in bestelling.GetFinalTax( )) {
				productsGrid.Add(new GridRow(new TextBlock(kp.Key) { ColSpan = 4 }, new TextBlock($"€ +{kp.Value:f2}") { ColSpan = 2 }));
			}

			// totaal prijs
			productsGrid.Add(new GridRow(new TextBlock("Totaal") { ColSpan = 4 }, new TextBlock($"€ {bestelling.TotaalPrijsWithTax:f2}") { ColSpan = 2 }));

			return productsGrid;
		}
	}
}
