﻿using Facturatiesysteem.data;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Facturatiesysteem.windows.BestellingOverviewWindow {
	class BestellingOverviewViewModel : ViewModelBase {
		//
		// Fields
		//
		private List<Bestelling> _bestellingen;
		private Bestelling _selectedBestelling;

		//
		// Constructors
		//
		public BestellingOverviewViewModel(Personeelslid personeelslid) {
			Personeelslid = personeelslid;

			SaveFactuurCommand = new Command(new Action<object>(SaveFactuur));
			SaveAndOpenFactuurCommand = new Command(new Action<object>(SaveAndOpenFactuur));

			RefreshAll( );
		}

		//
		// Properties
		//
		public Personeelslid Personeelslid { get; private set; }

		public Command SaveFactuurCommand { get; set; }
		public Command SaveAndOpenFactuurCommand { get; set; }

		public List<Bestelling> Bestellingen {
			get => _bestellingen;
			set {
				_bestellingen = value;
				NotifyPropertyChanged( );
			}
		}
		public Bestelling SelectedBestelling {
			get => _selectedBestelling;
			set {
				_selectedBestelling = value;
				NotifyPropertyChanged( );
			}
		}

		//
		// Methodes
		//
		public void RefreshAll( ) {
			Bestellingen = DataManager.GetAllBestellingen( ).OrderByDescending(b => b.BestellingNummer).ToList( );
		}

		public void SaveFactuur(object obj) {
			if (SelectedBestelling is null) {
				MessageBox.Show("Je hebt geen bestelling geselecteerd", "Fout");
				return;
			}
			SaveFileDialog dialog = new SaveFileDialog {
				CheckPathExists = true,
				Filter = "Alles (.*)|*.*|PDF (.pdf)|*.pdf",
				FilterIndex = 2,
				DefaultExt = ".pdf",
				InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
			};
			if (dialog.ShowDialog( ) == true) {
				if (SelectedBestelling.DatumFactuur is null) {
					SelectedBestelling.DatumFactuur = DateTime.Now;
					DataManager.UpdateBestelling(SelectedBestelling);
				}
				DocumentGenerator.GenerateInvoiceAndSave(dialog.FileName, SelectedBestelling);
			}
		}

		public void SaveAndOpenFactuur(object obj) {
			if (SelectedBestelling is null) {
				MessageBox.Show("Je hebt geen bestelling geselecteerd", "Fout");
				return;
			}
			SaveFileDialog dialog = new SaveFileDialog {
				CheckPathExists = true,
				Filter = "Alles (.*)|*.*|PDF (.pdf)|*.pdf",
				FilterIndex = 2,
				DefaultExt = ".pdf",
				InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
			};
			if (dialog.ShowDialog( ) == true) {
				if (SelectedBestelling.DatumFactuur is null) {
					SelectedBestelling.DatumFactuur = DateTime.Now;
					DataManager.UpdateBestelling(SelectedBestelling);
				}
				DocumentGenerator.GenerateInvoiceAndSave(dialog.FileName, SelectedBestelling);
				System.Diagnostics.Process.Start(dialog.FileName);
			}
		}
	}
}
