﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem {
	class Validation {
		//
		// Fields
		//

		//
		// Constructors
		//

		//
		// Properties
		//

		//
		// Methodes
		//
		public static string StringMayNotBeEmpty(object s)
			=> string.IsNullOrWhiteSpace((string)s) ? "Geef een tekst in" : null;

		public static string StringMustBeNumber(object s) {
			if (string.IsNullOrWhiteSpace((string)s))
				return "Geef een waarde in!";
			if (double.TryParse((string)s, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out _))
				return null;
			else
				return "Geef een nummer in!";
		}

		public static string StringMustBeNumberOrNull(object s) {
			if (string.IsNullOrWhiteSpace((string)s))
				return null;
			if (double.TryParse((string)s, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out _))
				return null;
			else
				return "Geef een nummer in!";
		}

		public static string StringMustBeInt(object s) {
			if (string.IsNullOrWhiteSpace((string)s))
				return "Geef een waarde in!";
			if (int.TryParse((string)s, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out _))
				return null;
			else
				return "Geef een nummer in!";
		}
	}
}
