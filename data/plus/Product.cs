﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem.data {
	public partial class Product {
		public string BTWPercent => $"{BTW * 100:F1} %";

		public override string ToString( ) => Naam;
	}
}
