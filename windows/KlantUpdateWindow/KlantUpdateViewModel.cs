﻿using Facturatiesysteem.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Facturatiesysteem {
	class KlantUpdateViewModel : ViewModelBase {

		//
		// Constructor
		//
		public KlantUpdateViewModel(Klant k) {
			KlantNieuwModel = new KlantUpdateModel(k);
			UpdateKlantCommand = new Command(new Action<object>(UpdateKlant));
			AnnuleerCommand = new Command(new Action<object>(Annuleer));
		}

		//
		// Properties
		//
		private KlantUpdateModel KlantNieuwModel { get; set; }
		public Command UpdateKlantCommand { get; set; }
		public Command AnnuleerCommand { get; set; }

		#region linking inputs
		public string BTWNummer {
			get => KlantNieuwModel.Klant.BTWNummer;
			set {
				KlantNieuwModel.Klant.BTWNummer = value;
				NotifyPropertyChanged( );
			}
		}
		public string Voornaam {
			get => KlantNieuwModel.Klant.Voornaam;
			set {
				KlantNieuwModel.Klant.Voornaam = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Achternaam {
			get => KlantNieuwModel.Klant.Achternaam;
			set {
				KlantNieuwModel.Klant.Achternaam = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Straat {
			get => KlantNieuwModel.Klant.Adres.Straat;
			set {
				KlantNieuwModel.Klant.Adres.Straat = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Huisnummer {
			get => KlantNieuwModel.Klant.Adres.Huisnummer;
			set {
				KlantNieuwModel.Klant.Adres.Huisnummer = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Bus {
			get => KlantNieuwModel.Klant.Adres.Bus;
			set {
				KlantNieuwModel.Klant.Adres.Bus = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Postcode {
			get => KlantNieuwModel.Klant.Adres.Postcode;
			set {
				KlantNieuwModel.Klant.Adres.Postcode = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Gemeente {
			get => KlantNieuwModel.Klant.Adres.Gemeente;
			set {
				KlantNieuwModel.Klant.Adres.Gemeente = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Telefoon {
			get => KlantNieuwModel.Klant.Telefoonnummer;
			set {
				KlantNieuwModel.Klant.Telefoonnummer = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Email {
			get => KlantNieuwModel.Klant.Emailadres;
			set {
				KlantNieuwModel.Klant.Emailadres = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Opmerking {
			get => KlantNieuwModel.Klant.Opmerking;
			set {
				KlantNieuwModel.Klant.Opmerking = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		#endregion

		//
		// Methodes
		//
		public void UpdateKlant(object window) {
			if (!KlantNieuwModel.UpdateKlant( ))
				MessageBox.Show("Er is iets mis gegaan");
			((Window)window).Close( );
		}
		public void Annuleer(object window) {
			((Window)window).Close( );
		}
	}
}
