﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem {
	abstract class ViewModelBase : INotifyPropertyChanged, IDataErrorInfo {
		public Dictionary<string, Func<object, string>> ErrorCheckList { get; } = new Dictionary<string, Func<object, string>>( );

		public string this[string columnName] => !ErrorCheckList.ContainsKey(columnName) ?
			null : ErrorCheckList[columnName].Invoke(GetType( ).GetProperty(columnName).GetValue(this, null));

		public string Error => throw new NotImplementedException( );

		public event PropertyChangedEventHandler PropertyChanged;
		protected void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = "") {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
