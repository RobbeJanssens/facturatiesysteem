﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem.data {
	public partial class Group {

		private Dictionary<int, Permission> PermissionDefention { get; } = new Dictionary<int, Permission>( ) {
			{0, Permission.Admin },
			{1, Permission.InsertKlant },
			{2, Permission.UpdateKlant },
			{3, Permission.DeleteKlant },
			{4, Permission.InsertCategorie },
			{5, Permission.UpdateCategorie },
			{6, Permission.DeleteCategorie },
			{7, Permission.InsertProduct },
			{8, Permission.UpdateProduct },
			{9, Permission.DeleteProduct },
			{10, Permission.CreateBestelling },
			{11, Permission.InsertKorting },
			{12, Permission.UpdateKorting },
			{13, Permission.DeleteKorting }
		};

		public HashSet<Permission> PermissionList => GetPemissions( );

		private BitArray PermissionBits => new BitArray(Permissions);

		public HashSet<Permission> GetPemissions( ) {
			HashSet<Permission> permissions = new HashSet<Permission>();

			foreach (KeyValuePair<int, Permission> keyValuePair in PermissionDefention) {
				if (PermissionBits[keyValuePair.Key]) permissions.Add(keyValuePair.Value);
			}

			return permissions;
		}

		/// <summary>
		/// Checks if a user has a certain permssion, or admin rights
		/// </summary>
		/// <param name="perm">Permission enum type</param>
		/// <returns>True if the user has the required right otherwise false</returns>
		public bool HasPermission(Permission perm) {
			if (!PermissionDefention.Values.Contains(perm)) return false;
			if (PermissionBits[PermissionDefention.First(x => x.Value == Permission.Admin).Key]) return true;
			return PermissionBits[PermissionDefention.First(x => x.Value == perm).Key];
		}
	}

	public enum Permission {
		Admin,
		InsertKlant,
		UpdateKlant,
		DeleteKlant,
		InsertCategorie,
		UpdateCategorie,
		DeleteCategorie,
		InsertProduct,
		UpdateProduct,
		DeleteProduct,
		CreateBestelling,
		InsertKorting,
		UpdateKorting,
		DeleteKorting
	}
}
