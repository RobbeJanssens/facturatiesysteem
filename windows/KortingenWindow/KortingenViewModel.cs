﻿using Facturatiesysteem.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Facturatiesysteem.windows.KortingenWindow {
	class KortingenViewModel : ViewModelBase {
		//
		// Fields
		//
		private List<Korting> _kortingen;
		private Korting _selectedKorting;
		private string _kortingCode;
		private decimal _kortingAmount;
		private List<Categorie> _categorieen;
		private List<Product> _producten;
		private Categorie _selectedCategorie;
		private Product _selectedProduct;
		private int _selectedProductIndex;
		private int _selectedCategorieIndex;
		private string _beschrijving;

		//
		// Constructors
		//
		public KortingenViewModel(Personeelslid personeelslid) {
			Personeelslid = personeelslid;

			RefreshCommand = new Command(new Action<object>(Refresh));
			CopyTextCommand = new Command(new Action<object>(CopyText));
			CloseCommand = new Command(new Action<object>(Close));
			ClearCategorieCommand = new Command(new Action<object>(ClearCategorie));
			ClearProductCommant = new Command(new Action<object>(ClearProduct));
			InsertNewKortingCommand = new Command(new Action<object>(InsertKorting));
			UpdateKortingCommand = new Command(new Action<object>(UpdateKorting));
			DeleteKortingCommand = new Command(new Action<object>(DeleteKorting));

			RefreshAll( );
		}

		//
		// Properties
		//
		public Personeelslid Personeelslid { get; private set; }

		public Command RefreshCommand { get; set; }
		public Command CopyTextCommand { get; set; }
		public Command CloseCommand { get; set; }
		public Command InsertNewKortingCommand { get; set; }
		public Command UpdateKortingCommand { get; set; }
		public Command DeleteKortingCommand { get; set; }
		public Command ClearCategorieCommand { get; set; }
		public Command ClearProductCommant { get; set; }

		#region bindings
		public int SelectedCategorieIndex {
			get => _selectedCategorieIndex;
			set {
				_selectedCategorieIndex = value;
				NotifyPropertyChanged( );
			}
		}
		public int SelectedProductIndex {
			get => _selectedProductIndex;
			set {
				_selectedProductIndex = value;
				NotifyPropertyChanged( );
			}
		}
		public List<Korting> Kortingen {
			get => _kortingen;
			set {
				_kortingen = value;
				NotifyPropertyChanged( );
			}
		}
		public List<Categorie> Categorieen {
			get => _categorieen;
			set {
				_categorieen = value;
				NotifyPropertyChanged( );
			}
		}
		public List<Product> Producten {
			get => _producten;
			set {
				_producten = value;
				NotifyPropertyChanged( );
			}
		}

		public Korting SelectedKorting {
			get => _selectedKorting;
			set {
				_selectedKorting = value;
				NotifyPropertyChanged( );
			}
		}

		public Categorie SelectedCategorie {
			get => _selectedCategorie;
			set {
				_selectedCategorie = value;
				NotifyPropertyChanged( );
			}
		}
		public Product SelectedProduct {
			get => _selectedProduct;
			set {
				_selectedProduct = value;
				NotifyPropertyChanged( );
			}
		}

		public string KortingCode {
			get => _kortingCode;
			set {
				_kortingCode = value;
				NotifyPropertyChanged( );
			}
		}
		public decimal KortingAmount {
			get => _kortingAmount;
			set {
				_kortingAmount = value;
				NotifyPropertyChanged( );
			}
		}
		public string Beschrijving {
			get => _beschrijving;
			set {
				_beschrijving = value;
				NotifyPropertyChanged( );
			}
		}
		#endregion

		//
		// Methodes
		//
		public void ClearCategorie(object obj) => SelectedCategorie = null;

		public void ClearProduct(object obj) => SelectedProduct = null;

		public void RefreshAll( ) {
			Kortingen = DataManager.GetAllKortingen( );
			Categorieen = DataManager.GetAllCategorieen( );
			Producten = DataManager.GetAllProducten( );
			KortingCode = "";
			KortingAmount = 0M;
		}

		public void Refresh(object obj) {
			RefreshAll( );
		}
		public void CopyText(object obj) {
			if (SelectedKorting is null)
				MessageBox.Show("Je hebt geen korting geselecteerd!", "Fout");
			else {
				KortingCode = SelectedKorting.KortingCode;
				KortingAmount = (decimal)(SelectedKorting.Hoeveelheid * 100M);
				SelectedCategorieIndex = Categorieen.FindIndex(c => c.CategorieID == SelectedKorting.CategorieID);
				SelectedProductIndex = Producten.FindIndex(p => p.ProductID == SelectedKorting.ProductID);
				Beschrijving = SelectedKorting.Beschrijving;
			}
		}
		public void Close(object window) {
			((Window)window).Close( );
		}
		public void InsertKorting(object obj) {
			if (!Personeelslid.Group.HasPermission(Permission.InsertKorting)) {
				MessageBox.Show("Je hebt geen toegang om kortingen aan te maken.", "Fout");
				return;
			}

			if (SelectedCategorie != null && SelectedProduct != null) {
				MessageBox.Show("Je kan niet zowel een categrie als een product aan een korting hangen.", "Fout");
			} else {
				Korting korting = new Korting {
					KortingID = Guid.NewGuid(),
					KortingCode = KortingCode,
					Hoeveelheid = KortingAmount / 100M,
					ProductID = SelectedProduct?.ProductID,
					CategorieID = SelectedCategorie?.CategorieID,
					Beschrijving = Beschrijving
				};
				DataManager.InsertKorting(korting);

				Kortingen = DataManager.GetAllKortingen( );

				KortingCode = null;
				KortingAmount = 0M;
				SelectedCategorie = null;
				SelectedProduct = null;
				Beschrijving = null;
			}
		}

		public void UpdateKorting(object obj) {
			if (!Personeelslid.Group.HasPermission(Permission.UpdateKorting)) {
				MessageBox.Show("Je hebt geen toegang om kortingen aan te passen.", "Fout");
				return;
			}

			if (SelectedKorting is null)
				MessageBox.Show("Je hebt geen korting geselecteerd!", "Fout");
			else if (SelectedCategorie != null && SelectedProduct != null) {
				MessageBox.Show("Je kan niet zowel een categrie als een product aan een korting hangen.", "Fout");
			} else {
				SelectedKorting.KortingCode = KortingCode;
				SelectedKorting.Hoeveelheid = KortingAmount / 100M;
				SelectedKorting.ProductID = SelectedProduct?.ProductID;
				SelectedKorting.CategorieID = SelectedCategorie?.CategorieID;
				SelectedKorting.Beschrijving = Beschrijving;

				DataManager.UpdateKorting(SelectedKorting);

				Kortingen = DataManager.GetAllKortingen( );

				KortingCode = null;
				KortingAmount = 0M;
				SelectedCategorie = null;
				SelectedProduct = null;
				Beschrijving = null;
			}
		}

		public void DeleteKorting(object obj) {
			if (!Personeelslid.Group.HasPermission(Permission.DeleteKorting)) {
				MessageBox.Show("Je hebt geen toegang om kortingen te verwijderen.", "Fout");
				return;
			}

			if (SelectedKorting is null)
				MessageBox.Show("Je hebt geen korting geselecteerd!", "Fout");
			else if (SelectedKorting.BestellingKortingen.Count > 0)
				MessageBox.Show("Je kan geen korting verwijderen die al gebruikt is!", "Fout");
			else {
				if (MessageBoxResult.OK == MessageBox.Show($"Ben je zeker dat je de korting met code {SelectedKorting.KortingCode} wilt verwijderen?", "Waarschuwing", MessageBoxButton.OKCancel))
					DataManager.DeleteKorting(SelectedKorting);
				RefreshAll( );
			}
		}
	}
}
