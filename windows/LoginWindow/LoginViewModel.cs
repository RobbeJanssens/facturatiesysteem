﻿using Facturatiesysteem.data;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Facturatiesysteem {
	class LoginViewModel : ViewModelBase {
		//
		// Fields
		//
		private string _gebruikersnaam;

		//
		// Constructors
		//
		public LoginViewModel( ) {
			ErrorCheckList["TxtGebruikersnaam"] = Validation.StringMayNotBeEmpty;
			OkCommand = new Command(new Action<object>(OkMethod));
			CancelCommand = new Command(new Action<object>(CancelMethod));
		}

		//
		// Properties
		//
		public string TxtGebruikersnaam {
			get => _gebruikersnaam;
			set {
				_gebruikersnaam = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public Command OkCommand { get; set; }
		public Command CancelCommand { get; set; }
		private LoginModel LoginModel { get; set; } = new LoginModel( );
		public LoginView LoginView { get; set; }

		public Personeelslid Personeelslid { get; private set; }

		//
		// Methodes
		//
		private void OkMethod(object pass) {
			PasswordBox passbox = (PasswordBox)pass;
			Personeelslid = LoginModel.CheckLogin(TxtGebruikersnaam, passbox.Password);
			if (Personeelslid == null) MessageBox.Show("Onjuiste gebruikersnaam en wachtwoord combinatie.", "Fout");
			else {
				LoginView.DialogResult = true;
				LoginView.Close( );
			}
		}
		private void CancelMethod(object window) {
			LoginView.DialogResult = false;
			((Window)window).Close( );
		}
	}
}
