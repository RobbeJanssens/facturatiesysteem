﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem.data {
	public partial class Bestelling {

		public string Datum => $"{DatumOpgemaakt:yyyy-MM-dd}";

		public decimal TotaalPrijs => GetTotaalPrijs( );

		// totaal zonder korting en tax
		public decimal Subtotaal => (decimal)BestellingProducten.Select(bp => bp.Aantal * bp.PrijsPerUnit).Sum( );

		public decimal TotaalPrijsWithTax => GetTotaalPrijs( ) + GetTotaalTax( );

		public decimal GetTotaalTax( ) {
			return GetFinalTax( ).Values.Sum( );
		}

		public Dictionary<string, decimal> GetFinalTax( ) {
			Dictionary<decimal, decimal> taxes = new Dictionary<decimal, decimal>();
			Dictionary<BestellingProduct, decimal> prices = GetProductPrices();

			foreach (KeyValuePair<BestellingProduct, decimal> kp in prices) {
				if (taxes.ContainsKey((decimal)kp.Key.Product.BTW))
					taxes[(decimal)kp.Key.Product.BTW] += (decimal)(kp.Value * kp.Key.Product.BTW);
				else
					taxes.Add((decimal)kp.Key.Product.BTW, (decimal)(kp.Value * kp.Key.Product.BTW));
			}

			Dictionary<string, decimal> strTaxes = new Dictionary<string, decimal>();
			foreach (KeyValuePair<decimal, decimal> kp in taxes)
				strTaxes.Add($"BTW {kp.Key * 100:f0}%", kp.Value);

			return strTaxes;
		}

		public Dictionary<Korting, decimal> GetKortingen( ) {
			List<Korting> overallKortings = BestellingKortingen.Select(bk => bk.Korting).Where(k => k.CategorieID is null && k.ProductID is null).ToList();
			List<Korting> categorieKortings = BestellingKortingen.Select(bk => bk.Korting).Where(k => k.CategorieID is not null).ToList();
			List<Korting> productenKortings = BestellingKortingen.Select(bk => bk.Korting).Where(k => k.ProductID is not null).ToList();

			Dictionary<Product, decimal> producten = new Dictionary<Product, decimal>();
			foreach (BestellingProduct bp in BestellingProducten)
				producten.Add(bp.Product, (decimal)(bp.Aantal * bp.PrijsPerUnit));

			Dictionary<Korting, decimal> kortingen = new Dictionary<Korting, decimal>();

			foreach (Korting k in overallKortings) {
				decimal totaal = 0M;
				foreach (Product p in producten.Keys.ToList( )) {
					decimal vermindering = (decimal)(k.Hoeveelheid * producten[p]);
					totaal += vermindering;
					producten[p] -= vermindering;
				}
				kortingen.Add(k, totaal);
			}
			foreach (Korting k in categorieKortings) {
				decimal totaal = 0M;
				foreach (Product p in producten.Keys.Where(p => p.CategorieID == k.CategorieID).ToList( )) {
					decimal vermindering = (decimal)(k.Hoeveelheid * producten[p]);
					totaal += vermindering;
					producten[p] -= vermindering;
				}
				kortingen.Add(k, totaal);
			}
			foreach (Korting k in productenKortings) {
				decimal totaal = 0M;
				foreach (Product p in producten.Keys.Where(p => p.ProductID == k.ProductID).ToList( )) {
					decimal vermindering = (decimal)(k.Hoeveelheid * producten[p]);
					totaal += vermindering;
					producten[p] -= vermindering;
				}
				kortingen.Add(k, totaal);
			}

			return kortingen;
		}

		private Dictionary<BestellingProduct, decimal> GetProductPrices( ) {
			decimal kortingOverall = 1M;
			Dictionary<Guid, decimal> kortingCategory = new Dictionary<Guid, decimal>();
			Dictionary<Guid, decimal> kortingProduct = new Dictionary<Guid, decimal>();

			foreach (BestellingKorting bk in BestellingKortingen) {
				if (bk.Korting.ProductID != null)
					if (kortingProduct.ContainsKey((Guid)bk.Korting.ProductID))
						kortingProduct[(Guid)bk.Korting.ProductID] *= 1 - (decimal)bk.Korting.Hoeveelheid;
					else
						kortingProduct.Add((Guid)bk.Korting.ProductID, 1 - (decimal)bk.Korting.Hoeveelheid);
				else if (bk.Korting.CategorieID != null)
					if (kortingCategory.ContainsKey((Guid)bk.Korting.CategorieID))
						kortingCategory[(Guid)bk.Korting.CategorieID] *= 1 - (decimal)bk.Korting.Hoeveelheid;
					else
						kortingCategory.Add((Guid)bk.Korting.CategorieID, 1 - (decimal)bk.Korting.Hoeveelheid);
				else
					kortingOverall *= (decimal)(1 - bk.Korting.Hoeveelheid);
			}

			Dictionary<BestellingProduct, decimal> prices = new Dictionary<BestellingProduct, decimal>();

			foreach (BestellingProduct bp in BestellingProducten) {
				decimal subtotaal = (decimal)(bp.PrijsPerUnit * bp.Aantal);
				subtotaal *= kortingOverall;
				if (kortingCategory.ContainsKey(bp.Product.CategorieID))
					subtotaal *= kortingCategory[bp.Product.CategorieID];
				if (kortingProduct.ContainsKey(bp.ProductID))
					subtotaal *= kortingProduct[bp.ProductID];
				prices.Add(bp, subtotaal);
			}

			return prices;
		}

		public decimal GetTotaalPrijs( ) {
			decimal totaal = 0M;
			foreach (decimal subtotaal in GetProductPrices( ).Values)
				totaal += subtotaal;

			return totaal;
		}
	}
}
