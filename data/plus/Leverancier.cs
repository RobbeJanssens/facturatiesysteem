﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem.data {
	public partial class Leverancier {
		public override string ToString( ) => Naam;
	}
}
