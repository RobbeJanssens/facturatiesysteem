﻿using Facturatiesysteem.data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem {
	/// <summary>
	/// A place to put all methodes to interact with the database
	/// </summary>
	class DataManager {
		#region Personeelslid
		/// <summary>
		///	Get the employee of a given login
		/// </summary>
		/// <param name="login">login name of an employee</param>
		/// <returns>Personeelslid or eplmoyee</returns>
		public static Personeelslid GetPersoneelslidByLogin(string login) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			return db.Personeelsleden.Include("Group").Where(p => p.Login == login).FirstOrDefault( );
		}
		#endregion

		#region Klanten
		public static List<Klant> GetAllKlanten( ) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			return db.Klanten.Include("Adres").ToList( );
		}
		public static List<Klant> GetAllKlantenWithBestellingen( ) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			return db.Klanten.Include("Adres").Include("Bestellingen").ToList( );
		}
		public static Klant GetKlantByID(Guid guid) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			return db.Klanten.Include("Adres").Where(k => k.KlantID == guid).FirstOrDefault( );
		}
		public static Klant GetKlantWithBestellingenByID(Guid guid) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			return db.Klanten.Include("Adres").Include("Bestellingen").Where(k => k.KlantID == guid).FirstOrDefault( );
		}
		public static bool InsertKlant(Klant k) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Klanten.Add(k);
			return db.SaveChanges( ) > 0;
		}
		public static bool UpdateKlant(Klant k) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Entry(k).State = System.Data.Entity.EntityState.Modified;
			return db.SaveChanges( ) > 0;
		}
		public static bool DeleteKlant(Klant k) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Entry(k).State = System.Data.Entity.EntityState.Deleted;
			return db.SaveChanges( ) > 0;
		}
		#endregion

		#region Adres
		public static bool InsertAdres(Adres a) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Adressen.Add(a);
			return db.SaveChanges( ) > 0;
		}
		public static bool UpdateAdres(Adres a) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Entry(a).State = System.Data.Entity.EntityState.Modified;
			return db.SaveChanges( ) > 0;
		}
		#endregion

		#region Categorieën
		public static List<Categorie> GetAllCategorieen( ) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			return db.Categorieen.ToList( );
		}
		public static bool InsertCategorie(Categorie c) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Categorieen.Add(c);
			return db.SaveChanges( ) > 0;
		}
		public static bool UpdateCategorie(Categorie c) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Entry(c).State = System.Data.Entity.EntityState.Modified;
			return db.SaveChanges( ) > 0;
		}
		public static bool DeleteCategorie(Categorie c) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Entry(c).State = System.Data.Entity.EntityState.Deleted;
			return db.SaveChanges( ) > 0;
		}
		#endregion

		#region Producten
		public static List<Product> GetAllProductenFromCategorie(Categorie c) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			return db.Producten.Where(b => b.Categorie.CategorieID == c.CategorieID).ToList( );
		}
		public static List<Product> GetAllProducten( ) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			return db.Producten.Include("Categorie").ToList( );
		}
		public static bool DeleteProduct(Product p) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Entry(p).State = System.Data.Entity.EntityState.Deleted;
			return db.SaveChanges( ) > 0;
		}
		public static bool InsertProdcut(Product p) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Entry(p).State = System.Data.Entity.EntityState.Added;
			return db.SaveChanges( ) > 0;
		}
		public static bool UpdateProdcut(Product p) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Entry(p).State = System.Data.Entity.EntityState.Modified;
			return db.SaveChanges( ) > 0;
		}
		#endregion

		#region BestellingProducten
		public static int GetBoundBestellingProductCountFromProduct(Product p) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			return db.BestellingProducten.Include("Product.Categorie").Where(bp => bp.ProductID == p.ProductID).Count( );
		}
		#endregion

		#region Bestellingen
		public static bool InsertBestelling(Bestelling b) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			b.BestellingProducten.ToList( ).ForEach(bp => bp.ProductID = bp.Product is null ? bp.ProductID : bp.Product.ProductID);
			b.BestellingKortingen.ToList( ).ForEach(bk => bk.KortingID = bk.Korting is null ? bk.KortingID : bk.Korting.KortingID);
			b.BestellingProducten.ToList( ).ForEach(bp => bp.Product = null);
			b.BestellingKortingen.ToList( ).ForEach(bk => bk.Korting = null);
			b.KlantID = b.Klant.KlantID;
			b.Klant = null;
			db.Bestellingen.Add(b);
			return db.SaveChanges( ) > 0;
		}
		public static Bestelling GetBestellingFromID(Guid id) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			return db.Bestellingen.Include("BestellingProducten.Product").Include("Klant.Adres").Include("BestellingKortingen.Korting.Product").Include("Personeelslid")
				.Where(b => b.BestellingID == id).FirstOrDefault( );
		}
		public static Bestelling GetBestellingFromBestellingnummer(int num) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			return db.Bestellingen.Include("BestellingProducten.Product").Include("Klant.Adres").Include("BestellingKortingen.Korting.Product").Include("Personeelslid")
				.Where(b => b.BestellingNummer == num).FirstOrDefault( );
		}
		public static List<Bestelling> GetAllBestellingen( ) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			return db.Bestellingen.Include("BestellingProducten.Product").Include("Klant.Adres").Include("BestellingKortingen.Korting.Product").Include("Personeelslid").ToList( );
		}
		public static bool UpdateBestelling(Bestelling b) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Entry(b).State = System.Data.Entity.EntityState.Modified;
			return db.SaveChanges( ) > 0;
		}
		#endregion

		#region Kortingen 
		public static List<Korting> GetAllKortingen( ) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			return db.Kortingen.Include("Product").Include("Categorie").Include("BestellingKortingen").ToList( );
		}
		public static bool InsertKorting(Korting k) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Kortingen.Add(k);
			return db.SaveChanges( ) > 0;
		}
		public static bool UpdateKorting(Korting k) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Entry(k).State = System.Data.Entity.EntityState.Modified;
			return db.SaveChanges( ) > 0;
		}
		public static bool DeleteKorting(Korting k) {
			using FacturatiesysteemDBEntities db = new FacturatiesysteemDBEntities( );
			db.Entry(k).State = System.Data.Entity.EntityState.Deleted;
			return db.SaveChanges( ) > 0;
		}
		#endregion
	}
}
