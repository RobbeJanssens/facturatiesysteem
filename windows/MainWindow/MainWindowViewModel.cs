﻿using Facturatiesysteem.data;
using Facturatiesysteem.windows.BestellingNieuwWindow;
using Facturatiesysteem.windows.BestellingOverviewWindow;
using Facturatiesysteem.windows.CategorieWindow;
using Facturatiesysteem.windows.KlantWindow;
using Facturatiesysteem.windows.KortingenWindow;
using Facturatiesysteem.windows.ProdutenWindow;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Facturatiesysteem {
	// TODO: add overview to main window
	class MainWindowViewModel : ViewModelBase {
		//
		// Fields
		//

		//
		// Constructors
		//
		public MainWindowViewModel( ) {
			LoginCommand = new Command(new Action<object>(Login));
			CloseCommand = new Command(new Action<object>(Close));
			OpenKlantenWindowCommand = new Command(new Action<object>(OpenKlantenWindow));
			OpenCategorieWindowCommand = new Command(new Action<object>(OpenCategorieWindow));
			OpenProductenWindowCommand = new Command(new Action<object>(OpenProductenWindow));
			OpenBestellingNieuwWindowCommand = new Command(new Action<object>(OpenBestellingNieuwWindow));
			OpenBestellingOverviewWindowCommand = new Command(new Action<object>(OpenBestellingOverviewWindow));
			OpenKortingenWindowCommand = new Command(new Action<object>(OpenKortingenWindow));
		}

		//
		// Properties
		//
		public Personeelslid Personeelslid { get; set; }
		public Command LoginCommand { get; set; }
		public Command CloseCommand { get; set; }
		public Command OpenKlantenWindowCommand { get; set; }
		public Command OpenCategorieWindowCommand { get; set; }
		public Command OpenProductenWindowCommand { get; set; }
		public Command OpenBestellingNieuwWindowCommand { get; set; }
		public Command OpenBestellingOverviewWindowCommand { get; set; }
		public Command OpenKortingenWindowCommand { get; set; }

		public List<Window> OpenWindows => GetOpenWindowList( );

		//
		// Methodes
		//
		public List<Window> GetOpenWindowList( ) {
			List<Window> wl = new List<Window>();
			foreach (Window w in Application.Current.Windows)
				wl.Add(w);
			return wl;
		}

		public void Login(object window) {
			OpenWindows.Where(w => w != window).ToList( ).ForEach(w => w.Close( ));
			LoginViewModel vm = new LoginViewModel();
			LoginView view = new LoginView() {DataContext = vm};
			vm.LoginView = view;

			if (view.ShowDialog( ) == true) {
				Personeelslid = vm.Personeelslid;
			} else if (Personeelslid is null) {
				Environment.Exit(0);
			}
		}

		public void OpenKlantenWindow(object obj) {
			List<Window> windows = OpenWindows.Where(w => w.GetType( ) == typeof(KlantView)).ToList();
			if (windows.Count( ) > 0) {
				MessageBoxResult result = MessageBox.Show("Er is al een klanten venster open, wilt u deze sluiten?", "Waarschuwing", MessageBoxButton.OKCancel);
				if (result == MessageBoxResult.OK) {
					foreach (Window window in windows.ToArray( ))
						window.Close( );
					CreateKlantenWindow( );
				}
			} else
				CreateKlantenWindow( );
		}

		public void CreateKlantenWindow( ) {
			KlantViewModel vm = new KlantViewModel(Personeelslid);
			KlantView view = new KlantView() {DataContext = vm};
			view.Show( );
		}

		public void OpenCategorieWindow(object obj) {
			List<Window> windows = OpenWindows.Where(w => w.GetType( ) == typeof(CategorieView)).ToList();
			if (windows.Count( ) > 0) {
				MessageBoxResult result = MessageBox.Show("Er is al een categorie venster open, wilt u deze sluiten?", "Waarschuwing", MessageBoxButton.OKCancel);
				if (result == MessageBoxResult.OK) {
					foreach (Window window in windows.ToArray( ))
						window.Close( );
					CreateCategorieWindow( );
				}
			} else
				CreateCategorieWindow( );
		}

		public void CreateCategorieWindow( ) {
			CategorieViewModel vm = new CategorieViewModel(Personeelslid);
			CategorieView view = new CategorieView() {DataContext = vm};
			view.Show( );
		}

		public void OpenProductenWindow(object obj) {
			List<Window> windows = OpenWindows.Where(w => w.GetType( ) == typeof(ProductenView)).ToList();
			if (windows.Count( ) > 0) {
				MessageBoxResult result = MessageBox.Show("Er is al een producten venster open, wilt u deze sluiten?", "Waarschuwing", MessageBoxButton.OKCancel);
				if (result == MessageBoxResult.OK) {
					foreach (Window window in windows.ToArray( ))
						window.Close( );
					CreateProductenWindow( );
				}
			} else
				CreateProductenWindow( );
		}

		public void CreateProductenWindow( ) {
			ProductenViewModel vm = new ProductenViewModel(Personeelslid);
			ProductenView view = new ProductenView() {DataContext = vm};
			view.Show( );
		}

		public void OpenKortingenWindow(object obj) {
			List<Window> windows = OpenWindows.Where(w => w.GetType( ) == typeof(KortingenView)).ToList();
			if (windows.Count( ) > 0) {
				MessageBoxResult result = MessageBox.Show("Er is al een kortingen venster open, wilt u deze sluiten?", "Waarschuwing", MessageBoxButton.OKCancel);
				if (result == MessageBoxResult.OK) {
					foreach (Window window in windows.ToArray( ))
						window.Close( );
					CreateKortingenWindow( );
				}
			} else
				CreateKortingenWindow( );
		}

		public void CreateKortingenWindow( ) {
			KortingenViewModel vm = new KortingenViewModel(Personeelslid);
			KortingenView view = new KortingenView() {DataContext = vm};
			view.Show( );
		}

		public void OpenBestellingOverviewWindow(object obj) {
			List<Window> windows = OpenWindows.Where(w => w.GetType( ) == typeof(BestellingOverviewView)).ToList();
			if (windows.Count( ) > 0) {
				MessageBoxResult result = MessageBox.Show("Er is al een kortingen venster open, wilt u deze sluiten?", "Waarschuwing", MessageBoxButton.OKCancel);
				if (result == MessageBoxResult.OK) {
					foreach (Window window in windows.ToArray( ))
						window.Close( );
					CreateBestellingOverviewWindow( );
				}
			} else
				CreateBestellingOverviewWindow( );
		}

		public void CreateBestellingOverviewWindow( ) {
			BestellingOverviewViewModel vm = new BestellingOverviewViewModel(Personeelslid);
			BestellingOverviewView view = new BestellingOverviewView() {DataContext = vm};
			view.Show( );
		}

		public void OpenBestellingNieuwWindow(object obj) {
			if (!Personeelslid.Group.HasPermission(Permission.CreateBestelling)) {
				MessageBox.Show("Je hebt geen toegang om een bestelling te maken.", "Fout");
				return;
			}
			List<Window> windows = OpenWindows.Where(w => w.GetType( ) == typeof(BestellingNieuwView)).ToList();
			if (windows.Count( ) >= 3)
				MessageBox.Show("Er zijn al 3 nieuwe bestelling vensters open.", "Fout");
			else {
				BestellingNieuwViewModel vm = new BestellingNieuwViewModel(Personeelslid);
				BestellingNieuwView view = new BestellingNieuwView( ) { DataContext = vm };
				view.Show( );
			}
		}

		public void Close(object obj) {
			if (MessageBox.Show("Bent u zeker dat u wilt aflsuiten?", "Waarschuwing", MessageBoxButton.YesNo) == MessageBoxResult.Yes) {
				Environment.Exit(0);
			}
		}
	}
}
