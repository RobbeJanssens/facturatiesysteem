﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Facturatiesysteem.windows.KlantNieuwWindow {
	class KlantNieuwViewModel : ViewModelBase {
		//
		// Fields
		//

		//
		// Constructors
		//
		public KlantNieuwViewModel( ) {
			KlantNieuwModel = new KlantNieuwModel( );
			VoegNieuweKlantToeCommand = new Command(new Action<object>(VoegNieuweKlantToe));
			AnnuleerCommand = new Command(new Action<object>(Annuleer));
		}

		//
		// Properties
		//
		private KlantNieuwModel KlantNieuwModel { get; set; }
		public Command VoegNieuweKlantToeCommand { get; set; }
		public Command AnnuleerCommand { get; set; }

		#region linking inputs
		public string BTWNummer {
			get => KlantNieuwModel.Klant.BTWNummer;
			set {
				KlantNieuwModel.Klant.BTWNummer = value;
				NotifyPropertyChanged( );
			}
		}
		public string Voornaam {
			get => KlantNieuwModel.Klant.Voornaam;
			set {
				KlantNieuwModel.Klant.Voornaam = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Achternaam {
			get => KlantNieuwModel.Klant.Achternaam;
			set {
				KlantNieuwModel.Klant.Achternaam = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Straat {
			get => KlantNieuwModel.Klant.Adres.Straat;
			set {
				KlantNieuwModel.Klant.Adres.Straat = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Huisnummer {
			get => KlantNieuwModel.Klant.Adres.Huisnummer;
			set {
				KlantNieuwModel.Klant.Adres.Huisnummer = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Bus {
			get => KlantNieuwModel.Klant.Adres.Bus;
			set {
				KlantNieuwModel.Klant.Adres.Bus = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Postcode {
			get => KlantNieuwModel.Klant.Adres.Postcode;
			set {
				KlantNieuwModel.Klant.Adres.Postcode = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Gemeente {
			get => KlantNieuwModel.Klant.Adres.Gemeente;
			set {
				KlantNieuwModel.Klant.Adres.Gemeente = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Telefoon {
			get => KlantNieuwModel.Klant.Telefoonnummer;
			set {
				KlantNieuwModel.Klant.Telefoonnummer = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Email {
			get => KlantNieuwModel.Klant.Emailadres;
			set {
				KlantNieuwModel.Klant.Emailadres = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		public string Opmerking {
			get => KlantNieuwModel.Klant.Opmerking;
			set {
				KlantNieuwModel.Klant.Opmerking = value.Trim( );
				NotifyPropertyChanged( );
			}
		}
		#endregion

		//
		// Methodes
		//
		public void VoegNieuweKlantToe(object obj) {
			KlantNieuwModel.SaveNieuweKlant( );
			#region Notify preopery changed
			NotifyPropertyChanged("Voornaam");
			NotifyPropertyChanged("Achternaam");
			NotifyPropertyChanged("Straat");
			NotifyPropertyChanged("Huisnummer");
			NotifyPropertyChanged("Bus");
			NotifyPropertyChanged("Postcode");
			NotifyPropertyChanged("Gemeente");
			NotifyPropertyChanged("Telefoon");
			NotifyPropertyChanged("Email");
			NotifyPropertyChanged("Opmerking");
			NotifyPropertyChanged("BTWNummer");
			#endregion
		}

		public void Annuleer(object window) {
			((Window)window).Close( );
		}
	}
}
