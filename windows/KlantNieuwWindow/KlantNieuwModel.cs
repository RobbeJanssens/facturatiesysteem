﻿using Facturatiesysteem.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem.windows.KlantNieuwWindow {
	class KlantNieuwModel {
		//
		// Fields
		//

		//
		// Constructors
		//
		public KlantNieuwModel( ) {
			Klant = new Klant( ) { Adres = new Adres( ) { AdresID = Guid.NewGuid( ) } };
		}

		//
		// Properties
		//
		public Klant Klant { get; private set; }

		//
		// Methodes
		//
		public void SaveNieuweKlant( ) {
			Klant.KlantID = Guid.NewGuid( );
			Klant.AangemaaktOp = DateTime.Now;
			DataManager.InsertKlant(Klant);

			Klant = new Klant( ) { Adres = new Adres( ) { AdresID = Guid.NewGuid( ) } };
		}
	}
}
