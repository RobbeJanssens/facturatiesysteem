﻿using Facturatiesysteem.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Facturatiesysteem.windows.CategorieWindow {
	class CategorieViewModel : ViewModelBase {
		//
		// Fields
		//
		private List<Categorie> _categorieen;
		private string _txtNaam;
		private string _txtBeschrijving;

		//
		// Constructors
		//
		public CategorieViewModel(Personeelslid personeelslid) {
			Personeelslid = personeelslid;
			CloseCommand = new Command(new Action<object>(Close));
			InsertNewCategorieCommand = new Command(new Action<object>(InsertNewCategorie));
			RefreshCommand = new Command(new Action<object>(Refresh));
			CopyTextCommand = new Command(new Action<object>(CopyText));
			UpdateCategorieCommand = new Command(new Action<object>(UpdateCategorie));
			DeleteCategorieCommand = new Command(new Action<object>(DeleteCategorie));
			Categorieen = DataManager.GetAllCategorieen( );
		}

		//
		// Properties
		//
		public List<Categorie> Categorieen {
			get => _categorieen;
			set {
				_categorieen = value;
				NotifyPropertyChanged( );
			}
		}
		public Personeelslid Personeelslid { get; private set; }
		public Command RefreshCommand { get; set; }
		public Command CloseCommand { get; set; }
		public Command CopyTextCommand { get; set; }
		public Command UpdateCategorieCommand { get; set; }
		public Command DeleteCategorieCommand { get; set; }
		public Command InsertNewCategorieCommand { get; set; }
		public string TxtNaam {
			get => _txtNaam;
			set {
				_txtNaam = value;
				NotifyPropertyChanged( );
			}
		}
		public string TxtBeschrijving {
			get => _txtBeschrijving;
			set {
				_txtBeschrijving = value;
				NotifyPropertyChanged( );
			}
		}

		//
		// Methodes
		//
		public void Close(object window) {
			((Window)window).Close( );
		}
		public void InsertNewCategorie(object obj) {
			if (!Personeelslid.Group.HasPermission(Permission.InsertCategorie)) {
				MessageBox.Show("Je hebt geen toegang om nieuwe categorieën toe te voegen.", "Fout");
				return;
			}
			if (string.IsNullOrWhiteSpace(TxtNaam)) {
				MessageBox.Show("Je hebt geent naam ingegeven!", "Fout");
				return;
			}
			Categorie categorie = new Categorie() {
				CategorieID = Guid.NewGuid(),
				Naam = TxtNaam,
				Beschrijving = TxtBeschrijving
			};

			DataManager.InsertCategorie(categorie);
			Categorieen = DataManager.GetAllCategorieen( );
			TxtNaam = "";
			TxtBeschrijving = "";
		}

		public void Refresh(object obj) {
			Categorieen = DataManager.GetAllCategorieen( );
			TxtBeschrijving = "";
			TxtNaam = "";
		}

		public void CopyText(object grid) {
			Categorie categorie = (Categorie)((DataGrid)grid).SelectedItem;
			if (categorie is null)
				MessageBox.Show("Je hebt geen categorie geselecteerd!", "Fout");
			else {
				TxtNaam = categorie.Naam;
				TxtBeschrijving = categorie.Beschrijving;
			}
		}

		public void UpdateCategorie(object grid) {
			if (!Personeelslid.Group.HasPermission(Permission.UpdateCategorie)) {
				MessageBox.Show("Je hebt geen toegang om een categorie aan te passen.", "Fout");
				return;
			}

			Categorie categorie = (Categorie)((DataGrid)grid).SelectedItem;
			if (categorie is null)
				MessageBox.Show("Je hebt geen categorie geselecteerd!", "Fout");
			else {
				categorie.Naam = TxtNaam;
				categorie.Beschrijving = TxtBeschrijving;
				DataManager.UpdateCategorie(categorie);

				TxtNaam = "";
				TxtBeschrijving = "";
				Categorieen = DataManager.GetAllCategorieen( );
			}
		}

		public void DeleteCategorie(object grid) {
			if (!Personeelslid.Group.HasPermission(Permission.DeleteCategorie)) {
				MessageBox.Show("Je hebt geen toegang om een categorie te verwijderen.", "Fout");
				return;
			}

			Categorie categorie = (Categorie)((DataGrid)grid).SelectedItem;
			if (categorie is null)
				MessageBox.Show("Je hebt geen categorie geselecteerd!", "Fout");
			else {
				List<Product> products = DataManager.GetAllProductenFromCategorie(categorie);
				if (products.Count > 0)
					MessageBox.Show($"Je kan die categorie niet verwijderen want de volgende producten zitten er in: {string.Join(", ", products.Select(p => p.Naam))}");
				else {
					DataManager.DeleteCategorie(categorie);
					Categorieen = DataManager.GetAllCategorieen( );
				}
			}
		}
	}
}
