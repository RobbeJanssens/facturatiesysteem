﻿using System;
using System.Windows.Input;

namespace Facturatiesysteem {

	internal class Command : ICommand {
		private readonly Action<object> WhattoExecute;
		private readonly Func<bool> WhentoExecute;

		public Command(Action<object> What, Func<bool> When) // Point 1
		{
			WhattoExecute = What;
			WhentoExecute = When;
		}

		public Command(Action<object> What) // Point 1
		{
			WhattoExecute = What;
		}

		public bool CanExecute(object parameter) //Komt van interface.
		{
			if (WhentoExecute == null) {
				return true;
			}
			return WhentoExecute( ); // Point 2
		}

		public void Execute(object parameter) {
			WhattoExecute(parameter); // Point 3
		}

		public event EventHandler CanExecuteChanged
		{
			add { CommandManager.RequerySuggested += value; }
			remove { CommandManager.RequerySuggested -= value; }
		}
	}
}