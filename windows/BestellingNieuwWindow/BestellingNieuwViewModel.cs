﻿using Facturatiesysteem.data;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Facturatiesysteem.windows.BestellingNieuwWindow {
	class BestellingNieuwViewModel : ViewModelBase {
		//
		// Fields
		//
		private ObservableCollection<Product> _producten;
		private Product _selectedListboxItem;
		private BestellingProductViewModel _bestellingProductViewModel;
		private List<Klant> _klanten;
		private ObservableCollection<Korting> _kortingen;
		private ObservableCollection<Korting> _addedKortingen;
		private Korting _selectedKortingCMB;
		private Korting _selectedKortingGrid;
		private Klant _selectedKlant;

		//
		// Constructors
		//
		public BestellingNieuwViewModel(Personeelslid personeelslid) {
			Personeelslid = personeelslid;
			Bestelling = new Bestelling { BestellingID = Guid.NewGuid( ), PersoneelslidID = personeelslid.PersoneelslidID };
			BestellingProducts = new ObservableCollection<BestellingProductViewModel>( );

			Producten = new ObservableCollection<Product>(DataManager.GetAllProducten( ));
			Klanten = DataManager.GetAllKlanten( ).OrderBy(k => k.Achternaam).ThenBy(k => k.Voornaam).ToList( );
			Kortingen = new ObservableCollection<Korting>(DataManager.GetAllKortingen( ).OrderBy(k => k.KortingCode).ThenBy(k => k.Hoeveelheid));
			AddedKortingen = new ObservableCollection<Korting>( );

			AddProductToBestellingCommand = new Command(new Action<object>(AddProductToBestelling));
			RemoveProductFromBestellingCommand = new Command(new Action<object>(RemoveProductFromBestelling));
			MakeBestellingCommand = new Command(new Action<object>(MakeBestelling));
			MakeBestellingAndSaveFactuurCommand = new Command(new Action<object>(MakeBestellingAndSaveFactuur));
			PreviewFactuurCommand = new Command(new Action<object>(PreviewFactuur));
			AddKortingCommand = new Command(new Action<object>(AddKorting));
			RemoveKortingCommand = new Command(new Action<object>(RemoveKorting));

			BestellingProducts.CollectionChanged += BestellingProducts_CollectionChanged;
			AddedKortingen.CollectionChanged += AddedKortingen_CollectionChanged;
		}

		//
		// Properties
		//
		public Personeelslid Personeelslid { get; private set; }

		public ObservableCollection<Product> Producten {
			get => _producten;
			set {
				_producten = value;
				NotifyPropertyChanged( );
			}
		}

		public List<Klant> Klanten {
			get => _klanten;
			set {
				_klanten = value;
				NotifyPropertyChanged( );
			}
		}

		public ObservableCollection<Korting> Kortingen {
			get => _kortingen;
			set {
				_kortingen = value;
				NotifyPropertyChanged( );
			}
		}

		public ObservableCollection<Korting> AddedKortingen {
			get => _addedKortingen;
			set {
				_addedKortingen = value;
				NotifyPropertyChanged( );
			}
		}

		public Korting SelectedKortingCMB {
			get => _selectedKortingCMB;
			set {
				_selectedKortingCMB = value;
				NotifyPropertyChanged( );
			}
		}

		public Korting SelectedKortingGrid {
			get => _selectedKortingGrid;
			set {
				_selectedKortingGrid = value;
				NotifyPropertyChanged( );
			}
		}

		public ObservableCollection<BestellingProductViewModel> BestellingProducts { get; set; }
		public Bestelling Bestelling { get; set; }

		public string Opmerking {
			get => Bestelling.Opmerking;
			set {
				Bestelling.Opmerking = value;
				NotifyPropertyChanged( );
			}
		}

		public Product SelectedListboxItem {
			get => _selectedListboxItem;
			set {
				_selectedListboxItem = value;
				NotifyPropertyChanged( );
			}
		}
		public BestellingProductViewModel SelectedBestellingProductViewModel {
			get => _bestellingProductViewModel;
			set {
				_bestellingProductViewModel = value;
				NotifyPropertyChanged( );
			}
		}

		public Command AddProductToBestellingCommand { get; set; }
		public Command RemoveProductFromBestellingCommand { get; set; }
		public Command MakeBestellingCommand { get; set; }
		public Command AddKortingCommand { get; set; }
		public Command RemoveKortingCommand { get; set; }
		public Command MakeBestellingAndSaveFactuurCommand { get; set; }
		public Command PreviewFactuurCommand { get; set; }

		public decimal TotaalPrijs => Bestelling.TotaalPrijs;
		public decimal TotaalPrijsWithTax => Bestelling.TotaalPrijsWithTax;

		public Klant SelectedKlant {
			get => _selectedKlant;
			set {
				_selectedKlant = value;
				NotifyPropertyChanged( );
			}
		}

		public int Betaaltermein {
			get => Bestelling.FactuurBetaalTermein is null ? 0 : (int)Bestelling.FactuurBetaalTermein;
			set {
				Bestelling.FactuurBetaalTermein = value;
				NotifyPropertyChanged( );
			}
		}

		//
		// Methodes
		//
		public void AddProductToBestelling(object obj) {
			if (SelectedListboxItem is null) {
				MessageBox.Show("Je hebt geen product geselecteerd!", "Fout");
				return;
			}
			BestellingProduct bp = new BestellingProduct {
				BestellingProductID = Guid.NewGuid(),
				Product = SelectedListboxItem,
				ProductID = SelectedListboxItem.ProductID,
				PrijsPerUnit = SelectedListboxItem.Verkoopprijs,
				BestellingID = Bestelling.BestellingID,
				Aantal = 1
			};
			Bestelling.BestellingProducten.Add(bp);
			BestellingProductViewModel bpvm = new BestellingProductViewModel(bp);
			BestellingProducts.Add(bpvm);
			Producten.Remove(SelectedListboxItem);
		}
		public void RemoveProductFromBestelling(object obj) {
			if (SelectedBestellingProductViewModel is null) {
				MessageBox.Show("Je hebt geen product geselecteerd!", "Fout");
				return;
			}
			Product product = SelectedBestellingProductViewModel.Product;
			Producten.Add(product);
			SelectedListboxItem = product;
			Bestelling.BestellingProducten.Remove(SelectedBestellingProductViewModel.BestellingProduct);
			BestellingProducts.Remove(SelectedBestellingProductViewModel);
		}

		public void AddKorting(object obj) {
			if (SelectedKortingCMB is null)
				MessageBox.Show("Je hebt geen korting geselecteerd.", "Fout");
			else {
				BestellingKorting bk = new BestellingKorting {
					BestellingKortingID = Guid.NewGuid(),
					BestellingID = Bestelling.BestellingID,
					KortingID = SelectedKortingCMB.KortingID,
					Korting = SelectedKortingCMB
				};
				Bestelling.BestellingKortingen.Add(bk);
				AddedKortingen.Add(SelectedKortingCMB);
				Kortingen.Remove(SelectedKortingCMB);
			}
		}
		public void RemoveKorting(object obj) {
			if (SelectedKortingGrid is null)
				MessageBox.Show("Je hebt geen korting geselecteerd.", "Fout");
			else {
				Kortingen.Add(SelectedKortingGrid);
				BestellingKorting bk = Bestelling.BestellingKortingen.Where(x => x.KortingID == SelectedKortingGrid.KortingID).FirstOrDefault();
				Bestelling.BestellingKortingen.Remove(bk);
				AddedKortingen.Remove(SelectedKortingGrid);
			}
		}

		#region detecting property changed
		private void BestellingProducts_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {
			if (e.NewItems != null)
				foreach (BestellingProductViewModel bpvm in e.NewItems)
					bpvm.PropertyChanged += Bpvm_PropertyChanged;
			if (e.OldItems != null)
				foreach (BestellingProductViewModel bpvm in e.OldItems)
					bpvm.PropertyChanged -= Bpvm_PropertyChanged;
			NotifyPropertyChanged("TotaalPrijs");
			NotifyPropertyChanged("TotaalPrijsWithTax");
		}

		private void Bpvm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
			if (e.PropertyName == "Subtotal") {
				NotifyPropertyChanged("TotaalPrijs");
				NotifyPropertyChanged("TotaalPrijsWithTax");
			}
		}

		private void AddedKortingen_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {
			NotifyPropertyChanged("TotaalPrijs");
			NotifyPropertyChanged("TotaalPrijsWithTax");
		}
		#endregion.

		public void MakeBestelling(object window) {
			if (!IsKlantSelected( ))
				return;
			SaveBestellingToDB( );
			((Window)window).Close( );
		}

		private void SaveBestellingToDB( ) {
			Bestelling.DatumOpgemaakt = DateTime.Now;
			Bestelling.Klant = SelectedKlant;
			DataManager.InsertBestelling(Bestelling);
		}

		private bool IsKlantSelected( ) {
			if (SelectedKlant is null) {
				MessageBox.Show("Je hebt geen klant geselecteerd.", "Fout");
				return false;
			} else
				return true;
		}

		public void MakeBestellingAndSaveFactuur(object window) {
			if (!IsKlantSelected( ))
				return;
			SaveFileDialog dialog = new SaveFileDialog {
				CheckPathExists = true,
				Filter = "Alles (.*)|*.*|PDF (.pdf)|*.pdf",
				FilterIndex = 2,
				DefaultExt = ".pdf",
				InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
			};
			if (dialog.ShowDialog( ) == true) {
				Bestelling.DatumFactuur = DateTime.Now;
				SaveBestellingToDB( );

				Bestelling = DataManager.GetBestellingFromID(Bestelling.BestellingID);
				DocumentGenerator.GenerateInvoiceAndSave(dialog.FileName, Bestelling);
				System.Diagnostics.Process.Start(dialog.FileName);

				((Window)window).Close( );
			}
		}

		public void PreviewFactuur(object obj) {
			if (!IsKlantSelected( ))
				return;
			Bestelling.DatumOpgemaakt = DateTime.Now;
			Bestelling.Klant = SelectedKlant;

			// temp giving a date to the invoice
			Bestelling.DatumFactuur = DateTime.Now;

			string path = Environment.CurrentDirectory + @"\preview.pdf";
			DocumentGenerator.GenerateInvoiceAndSave(path, Bestelling);
			System.Diagnostics.Process.Start(path);

			// setting the date of the invoice back to null
			Bestelling.DatumFactuur = null;
		}
	}
}
