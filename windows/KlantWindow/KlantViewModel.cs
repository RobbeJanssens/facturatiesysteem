﻿using Facturatiesysteem.data;
using Facturatiesysteem.windows.KlantNieuwWindow;
using Facturatiesysteem.windows.KlantUpdateWindow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Facturatiesysteem.windows.KlantWindow {
	class KlantViewModel : ViewModelBase {
		//
		// Fields
		//
		private List<Klant> _klanten;

		//
		// Constructors
		//
		public KlantViewModel(Personeelslid personeelslid) {
			Personeelslid = personeelslid;
			RefreshAll( );
			OpenKlantNieuwWindowCommand = new Command(new Action<object>(OpenKlantNieuwWindow));
			OpenKlantUpdateWindowCommand = new Command(new Action<object>(OpenKlantUpdateWindow));
			DeleteKlantCommand = new Command(new Action<object>(DeleteKlant));
			RefreshCommand = new Command(new Action<object>(Refresh));
		}

		//
		// Properties
		//
		public List<Klant> Klanten {
			get => _klanten;
			set {
				_klanten = value;
				NotifyPropertyChanged( );
			}
		}
		public Command OpenKlantNieuwWindowCommand { get; set; }
		public Command OpenKlantUpdateWindowCommand { get; set; }
		public Command DeleteKlantCommand { get; set; }
		public Command RefreshCommand { get; set; }
		public Personeelslid Personeelslid { get; set; }

		//
		// Methodes
		//
		public void OpenKlantNieuwWindow(object obj) {
			if (Personeelslid.Group.HasPermission(Permission.InsertKlant)) {
				KlantNieuwViewModel vm = new KlantNieuwViewModel();
				KlantNieuwView v = new KlantNieuwView() {DataContext = vm};
				v.ShowDialog( );
				RefreshAll( );
			} else
				MessageBox.Show("Jij hebt geen toegang om nieuwe klanten toe te voegen.", "Fout");
		}

		public void RefreshAll( ) {
			Klanten = DataManager.GetAllKlanten( ).OrderBy(k => k.Achternaam).ThenBy(k => k.Voornaam).ToList( );
		}

		public void Refresh(object obj) {
			RefreshAll( );
		}
		public void OpenKlantUpdateWindow(object grid) {
			if (!Personeelslid.Group.HasPermission(Permission.UpdateKlant)) {
				MessageBox.Show("Jij hebt geen toegang om een klant te updaten.", "Fout");
				return;
			}

			Klant klant = (Klant)((DataGrid)grid).SelectedItem;
			if (klant is null)
				MessageBox.Show("Je hebt geen klant geslecteerd op aan te passen.", "Fout");
			else {
				KlantUpdateViewModel vm = new KlantUpdateViewModel(klant);
				KlantUpdateView view = new KlantUpdateView() {DataContext = vm };
				view.ShowDialog( );
				RefreshAll( );
			}
		}

		public void DeleteKlant(object grid) {
			if (!Personeelslid.Group.HasPermission(Permission.DeleteKlant)) {
				MessageBox.Show("Jij hebt geen toegang om een klant te deleten.", "Fout");
				return;
			}

			Klant klant = (Klant)((DataGrid)grid).SelectedItem;
			if (klant is null)
				MessageBox.Show("Je hebt geen klant geslecteerd op aan te passen.", "Fout");
			else {
				MessageBoxResult result = MessageBox.Show($"Bent u zeker dat u {klant.Naam} wilt verwijderen?", "Waarschuwing", MessageBoxButton.YesNoCancel);
				if (result == MessageBoxResult.Yes) {
					DataManager.DeleteKlant(klant);
					RefreshAll( );
				}
			}
		}
	}
}
