﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem.data {
	class BestellingProductViewModel : ViewModelBase {
		//
		// Fields
		//
		private BestellingProduct _bestellingProduct;

		//
		// Constructors
		//
		public BestellingProductViewModel( )
			: this(new BestellingProduct { BestellingProductID = Guid.NewGuid( ) }) {

		}
		public BestellingProductViewModel(BestellingProduct bp) {
			BestellingProduct = bp;
		}

		//
		// Properties
		//
		public BestellingProduct BestellingProduct {
			get => _bestellingProduct;
			set {
				_bestellingProduct = value;
				NotifyPropertyChanged( );
			}
		}

		public Guid BestellingProductID {
			get => BestellingProduct.BestellingProductID;
			set {
				BestellingProduct.BestellingProductID = value;
				NotifyPropertyChanged( );
			}
		}
		public Guid BestellingID {
			get => BestellingProduct.BestellingID;
			set {
				BestellingProduct.BestellingID = value;
				NotifyPropertyChanged( );
			}
		}
		public Guid ProductID {
			get => BestellingProduct.ProductID;
			set {
				BestellingProduct.ProductID = value;
				NotifyPropertyChanged( );
			}
		}
		public int Aantal {
			get => (int)BestellingProduct.Aantal;
			set {
				BestellingProduct.Aantal = value;
				NotifyPropertyChanged( );
				NotifyPropertyChanged("Subtotal");
			}
		}
		public decimal PrijsPerUnit {
			get => (int)BestellingProduct.PrijsPerUnit;
			set {
				BestellingProduct.PrijsPerUnit = value;
				NotifyPropertyChanged( );
				NotifyPropertyChanged("Subtotal");
			}
		}

		public virtual Bestelling Bestelling {
			get => BestellingProduct.Bestelling;
			set {
				BestellingProduct.Bestelling = value;
				NotifyPropertyChanged( );
			}
		}
		public virtual Product Product {
			get => BestellingProduct.Product;
			set {
				BestellingProduct.Product = value;
				NotifyPropertyChanged( );
			}
		}

		public string Opmerking {
			get => BestellingProduct.Opmerking;
			set {
				BestellingProduct.Opmerking = value;
				NotifyPropertyChanged( );
			}
		}

		public virtual Categorie Categorie {
			get => BestellingProduct.Product.Categorie;
			set {
				BestellingProduct.Product.Categorie = value;
				NotifyPropertyChanged( );
			}
		}

		public decimal Subtotal {
			get => BestellingProduct.Subtotal;
		}

		//
		// Methodes
		//
	}
}
