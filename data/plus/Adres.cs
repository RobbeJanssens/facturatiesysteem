﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem.data {
	public partial class Adres {
		public override string ToString( ) {
			if (string.IsNullOrWhiteSpace(Straat) &&
				string.IsNullOrWhiteSpace(Huisnummer) &&
				string.IsNullOrWhiteSpace(Bus) &&
				string.IsNullOrWhiteSpace(Gemeente) &&
				string.IsNullOrWhiteSpace(Postcode))
				return null;
			else return $"{Straat} {Huisnummer}{(string.IsNullOrWhiteSpace(Bus) ? "" : $" {Bus}")}, {Gemeente} {Postcode}";
		}
	}
}
