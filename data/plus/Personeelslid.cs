﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facturatiesysteem.data {
	public partial class Personeelslid {
		public override string ToString( ) {
			if (Voornaam is null)
				return null;
			else if (Achternaam is null)
				return Voornaam;
			else
				return $"{Voornaam} {Achternaam[0]}";
		}
	}
}
